﻿# ELASTIC.ISO
This is a custom 64-bit Ubuntu server .iso file that can be used to create and configure an elastic cluster. The custom .iso already has all of the componets needed to create a single or multi-node cluster ( Elasticsearch-7.6.2, Kibana-7.6.2 ), and a bash script that is used to automate the setup of the elastic cluster by adjusting the jvm.options, elasticsearch.yml, and hostname for you. The linux filesystem has been modified as well to help make using the setup script and creating a cluster faster and easier.

 

## Getting Started
- Download and save the latest version of the elastic.iso file.
- When creating a new VM select the elastic.iso file to be used as the bootable medium. 
- Follow the Ubuntu operating system installation instructions and be sure to congiure a static IP address.

## Running setup script
There are two avaiable options when running the setup script. 
- #### Elasticsearch Option
		setup -e
	- Running the elasticsearch setup option will allow for user input on creating a new elasticsearch node and adjusts the configuration settings accordianly. 
	- You have the ability to create a master, data, or ingest node using this setup option.


- #### Kibana Option
		setup -k
	- Running the kibana setup option will allow for user input on kibana server and adjusts the configuration settings accordianly.
	- This option also configures a coordinating-only elasticsearch node to act as a load-balancer for searches.
> **Note:** Make sure you create atleast one MASTER elasticsearch node and start it before settung up any other node/server.

## Starting the Elastic Cluster

#### Elasticsearch Node
	elasticsearch  or  ./elasticsearch

#### Kibana Server
	kibana  or  ./kibana
> **Note:** You can execute these commands from anywhere on the system.


# Use Case
## Creating a new Elasticsearch Node
### Master Node
- Creting a New Cluster
	- `setup -e`
	- What type of node is this (master/data/ingest): `master`
	- Are you join an exsiting cluster (y/n): `n`
	- Enter ip address of node: `[ip_of_system]`
- Joining an Existing Cluster
	- `setup -e`
	- What type of node is this (master/data/ingest): `master`
	- Are you join an exsiting cluster (y/n): `y`
	- Enter ip address of node: `[ip_of_system]`
	- Enter ip address of master node: `[ip_of_master_node]`
### Data Node
- Creating a new Data Node
	- `setup -e`
	-  What type of node is this (master/data/ingest): `data`
	- Enter ip address of node: `[ip_of_system]`
	- Enter ip address of master node: `[ip_of_master_node]`

### Ingest Node
- Creating a new Ingest Node
	- `setup -e`
	- What type of node is this (master/data/ingest): `ingest`
	- Enter ip address of node: `[ip_of_system]`
	- Enter ip address of master node: `[ip_of_master_node]`


## Creaing a new Kibana Server
- Creating a new Kibana Server
	- `setup -k`
	- Enter ip address of node: `[ip_of_system]`
	- Enter ip address of master node: `[ip_of_master_node]`




