#!/bin/bash
# Check to see if there is an setup option chosen
if [ $# -eq 0 ]; then
	echo "Please select an avaliable setup option (-e for elasticsearch, -k for kibana)"
	exit 0
fi

# Setup options to choose from
while [ "$1" ]; do
	case "$1" in
	# Elasticsearch setup option
	-e) 
		echo "ELASTICSEARCH SETUP"
		read -p "What type of node is this (master/data/ingest): " node
		case "$node" in
		# Master node setup
		master)
			read -p "Are you join an exsiting cluster (y/n): " new_master
			if [[ "$new_master" == "n" ]]; then
				read -p "Enter ip address of node: " network
				sudo hostname master1
				# Configure elasticsearch
				printf "node.name: master1\nnode.master: true\nnode.data: false\nnode.ingest: false\nnode.ml: false\nnetwork.host: \"$network\"\nhttp.port: 9200\ndiscovery.seed_hosts: [ \"127.0.0.1\" ]\ncluster.initial_master_nodes: [ \"master1\", \"master2\", \"master3\" ]" > /opt/elastic/elasticsearch-7.7.0/config/elasticsearch.yml
			else
				read -p "Enter ip address of node: " network
       				read -p "Enter ip address of known master nodes seperated by a comma: " master_node
				# Get name of all nodes in cluster
				nodes=$(curl -s -GET http://$master_node:9200/_cat/nodes?h=name)
				all_nodes=($nodes)
				master_node_num=1
				for node in ${all_nodes[@]}
				do
				  if [[ $node =~ "master" ]]; then
				    master_node_num=$((master_node_num+1))
				  fi
				done
				# Change hostname
				sudo hostname master$master_node_num
				printf "node.master: true\nnode.data: false\nnode.ingest: false\nnode.ml: false\nnetwork.host: \"$network\"\nhttp.port: 9200\ndiscovery.seed_hosts: [ \"$master_node\" ]\ncluster.initial_master_nodes: [ \"master1\", \"master2\", \"master3\" ]" > /opt/elastic/elasticsearch-7.7.0/config/elasticsearch.yml
			fi;;
		# Data node setup	
		data)
			read -p "Enter ip address of node: " network
			read -p "Enter ip address of master node: " master_node
			# Get name of all nodes in cluster
			nodes=$(curl -s -GET http://$master_node:9200/_cat/nodes?h=name)
			all_nodes=($nodes)
			data_node_num=1
			for node in ${all_nodes[@]}
			do
				if [[ $node =~ "data" ]]; then
					data_node_num=$((data_node_num+1))
				fi
			done
			# Change hostname
			sudo hostname data$data_node_num
			# Set elasticsearch.yml configs
			printf "node.ingest: false\nnode.ml: false\nnode.master: false\nnode.data: true\nnetwork.host: \"$network\"\nhttp.port: 9200\ndiscovery.seed_hosts: [ \"$master_node\" ]\ncluster.initial_master_nodes: [ \"master1\", \"master2\", \"master3\" ]" > /opt/elastic/elasticsearch-7.7.0/config/elasticsearch.yml;;
		# Ingest option
		ingest)	
			read -p "Enter ip address of node: " network
			read -p "Enter ip address of known master nodes seperated by a comma: " master_node
			# Get name of all nodes in cluster
			nodes=$(curl -s -GET http://$master_node:9200/_cat/nodes?h=name)
			all_nodes=($nodes)
			ingest_node_num=1
			for node in ${all_nodes[@]}
			do
				if [[ $node =~ "ingest" ]]; then
					ingest_node_num=$((ingest_node_num+1))
				fi
			done
			# Change hostname
			sudo hostname ingest$ingest_node_num
			# Set elasticsearch.yml configs
			printf "node.ingest: true\nnode.ml: false\nnode.master: false\nnode.data: false\nnetwork.host: \"$network\"\nhttp.port: 9200\ndiscovery.seed_hosts: [ \"$master_node\" ]\ncluster.initial_master_nodes: [ \"master1\", \"master2\", \"master3\" ]" > /opt/elastic/elasticsearch-7.7.0/config/elasticsearch.yml;;	
		# Error option
		*) 
			echo "Invaild Input. Pllease retry."
			exit 0;;
		esac
		# Get total size of memory
		mem=$(free -g | grep -oP '\d+' | head -n 1)
		# Create JVM Size (half size of memory) 
		jvmSize="$(((mem+1)/2))"
		# If jvm size is less than recommended mas of 30 set to jvmSize, else set to 30
		if (("$jvmSize" < 30)); then
			sed -i "/-Xms/c\-Xms$jvmSize\g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
			sed -i "/-Xmx/c\-Xmx$jvmSize\g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
		else
			sed -i "/-Xms/c\-Xms30g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
			sed -i "/-Xmx/c\-Xmx30g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
			fi;;

	# Kibana setup option
	-k) 
		echo "KIBANA SETUP"
		read -p "Enter ip address of known master nodes seperated by a comma: " master_node
		read -p "Enter ip address of node: " network
		sudo hostname coordinating-node
		# Configure coordinating elasticsearch node settings
		printf "node.name: coordinating-node\nnode.ingest: false\nnode.ml: false\nnode.master: false\nnode.data: false\nnetwork.host: \"$network\"\nhttp.port: 9200\ndiscovery.seed_hosts: [ \"$master_node\" ]\ncluster.initial_master_nodes: [ \"master1\", \"master2\", \"master3\" ]" > /opt/elastic/elasticsearch-7.7.0/config/elasticsearch.yml

		# Get total size of memory
		mem=$(free -g | grep -oP '\d+' | head -n 1)
		# Create JVM Size (half size of memory) 
		jvmSize="$(((mem+1)/2))"
		# If jvm size is less than recommended mas of 30 set to jvmSize, else set to 30
		if (("$jvmSize" < 30)); then
		 	sed -i "/-Xms/c\-Xms$jvmSize\g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
			sed -i "/-Xmx/c\-Xmx$jvmSize\g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
		else
		 	sed -i "/-Xms/c\-Xms30g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
			sed -i "/-Xmx/c\-Xmx30g" /opt/elastic/elasticsearch-7.7.0/config/jvm.options
		fi
		# Configure kibana settings
		printf "server.port: 5601\nserver.host: \"$network\"\nserver.name: \"kibana-server\"\nelasticsearch.hosts: \"http://$network:9200\"" > /opt/elastic/kibana-7.7.0-linux-x86_64/config/kibana.yml;;
	# Error option
	*) 
		echo "Please select an avaliable setup option (-e for elasticsearch, -k for kibana)"
		exit 0;;
	esac
	# Check for additonal options.
	shift
done
